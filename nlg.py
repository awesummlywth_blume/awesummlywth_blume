from pytldr.summarize.relevance import RelevanceSummarizer

# This object creates a summary using the summarize method:
# e.g. summarizer.summarize(text, length=5, weighting='frequency', norm=None)

# The length parameter specifies the length of the summary, either as a
# number of sentences, or a percentage of the original text

# The summarizer can take as input...
# 1. A string:
text = "Locals walk past a fallen power pole as Hurricane Irma moves off northern coast of Dominican Republic. Winds dipped on Thursday to 175 mph as the Irma soaked the northern coasts of the Dominican Republic and Haiti and brought hurricane-force wind to the Turks and Caicos Islands. It remained an extremely dangerous Category 5 storm, the highest designation by the National Hurricane Center (NHC). Irma was about 40 miles south of Turks and Caicos and is expected to reach the Bahamas later Thursday, before moving to Cuba and plowing into southern Florida as a very powerful Category 4 on Sunday, with storm surges and flooding due to begin within the next 48 hours. It was the first time the Turks and Caicos islands had experienced a Category 5 storm, said Virginia Clerveaux, director of Disaster Management and Emergencies. We are expecting inundation from both rainfall as well as storm surge. In the US Virgin islands, four people died, a government spokesman said, and a major hospital was badly damaged by the wind.A US amphibious assault ship arrived in the US Virgin Islands on Thursday and sent helicopters for medical evacuations from the destroyed hospital. Barbuda, where one person died, was reduced 'to rubble', according to Prime Minister Gaston Browne. In the British overseas territory of Anguilla another person was killed, while the hospital and airport, power and phone services were damaged, emergency service officials said."




#summarizer = LsaOzsoy()
#summarizer = LsaSteinberger()
#summarizer = LsaSummarizer()  # This is identical to the LsaOzsoy object
# summarizer = lsa.LsaSummarizer()
summarizer = RelevanceSummarizer()
summary = summarizer.summarize(
    text, length=2, binary_matrix=True
)

print(summary)