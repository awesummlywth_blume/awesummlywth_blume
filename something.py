from neuralcoref import Coref

stringInput = input("Enter the required text")

coref = Coref()
clusters = coref.one_shot_coref(utterances=stringInput)
print(clusters)

mentions = coref.get_mentions()
print(mentions)

resolved_utterance_text = coref.get_resolved_utterances()
print(resolved_utterance_text)
